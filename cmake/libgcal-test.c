#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int get_the_url(char *data, int length, char **url);

void xmlCleanupParser(void)
{
	// if this gets called, the libgcal we are using is broken
	exit(1);
}

int main()
{
	const char *xml = "<nothing></nothing>";
	char *dest;
	get_the_url((char*)xml, strlen(xml), &dest);
	return 0;
}

